package com.nagaroo.statement.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * The type Nagaroo statement service application.
 */
@SpringBootApplication
public class NagarooStatementServiceApplication implements WebMvcConfigurer {

    /**
     * mvn
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(NagarooStatementServiceApplication.class, args);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("/resources/static/");
        WebMvcConfigurer.super.addResourceHandlers(registry);
    }

}
