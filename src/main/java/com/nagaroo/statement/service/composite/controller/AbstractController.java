package com.nagaroo.statement.service.composite.controller;

import com.nagaroo.statement.service.core.dto.ExceptionDTO;
import com.nagaroo.statement.service.core.exception.InvalidIDataException;
import com.nagaroo.statement.service.core.exception.InvalidInputDataException;
import com.nagaroo.statement.service.core.exception.ItemNotFoundException;
import com.nagaroo.statement.service.core.exception.StatementAuthorizationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.time.format.DateTimeParseException;
import java.util.Date;

/**
 * The type Abstract controller.
 */
@Slf4j
@ControllerAdvice
public class AbstractController {


    /**
     * The constant TIMESTAMP.
     */
    public static final String TIMESTAMP = "timestamp";
    /**
     * The constant MESSAGE.
     */
    public static final String MESSAGE = "message";
    /**
     * The constant STATUS_CODE.
     */
    public static final String STATUS_CODE = "statusCode";
    /**
     * The constant EXCEPTION.
     */
    public static final String EXCEPTION = "exception";
    /**
     * The constant PATH.
     */
    public static final String PATH = "path";

    /**
     * Handle bad request exceptions response entity.
     *
     * @param exception the exception
     * @param request   the request
     * @return the response entity
     */
    @ExceptionHandler({ InvalidInputDataException.class, DateTimeParseException.class })
    public ResponseEntity<ExceptionDTO> handleBadRequestExceptions(RuntimeException exception,
        final HttpServletRequest request) {
        log.info("handleBadRequestExceptions() - invoked for Exception : {} ", exception.getClass());

        return new ResponseEntity<>(constructExceptionBody(exception, request, HttpStatus.BAD_REQUEST),
            HttpStatus.BAD_REQUEST);
    }

    /**
     * Handle invalid i data exception response entity.
     *
     * @param exception the exception
     * @param request   the request
     * @return the response entity
     */
    @ExceptionHandler({ InvalidIDataException.class })
    public ResponseEntity<ExceptionDTO> handleInvalidIDataException(RuntimeException exception,
        final HttpServletRequest request) {
        log.info("handleInvalidIDataException() - invoked");

        return new ResponseEntity<>(constructExceptionBody(exception, request, HttpStatus.UNPROCESSABLE_ENTITY),
            HttpStatus.UNPROCESSABLE_ENTITY);
    }

    /**
     * Handle item not found exception response entity.
     *
     * @param exception the exception
     * @param request   the request
     * @return the response entity
     */
    @ExceptionHandler({ ItemNotFoundException.class })
    public ResponseEntity<ExceptionDTO> handleItemNotFoundException(RuntimeException exception,
        final HttpServletRequest request) {
        log.info("handleItemNotFoundException() - invoked");

        return new ResponseEntity<>(constructExceptionBody(exception, request, HttpStatus.NOT_FOUND),
            HttpStatus.NOT_FOUND);
    }

    /**
     * Handle sql exception response entity.
     *
     * @param exception the exception
     * @param request   the request
     * @return the response entity
     */
    @ExceptionHandler({ SQLException.class })
    public ResponseEntity<ExceptionDTO> handleSQLException(Exception exception, final HttpServletRequest request) {
        log.info("handleSQLException() - invoked");

        return new ResponseEntity<>(constructExceptionBody(exception, request, HttpStatus.BAD_GATEWAY),
            HttpStatus.BAD_GATEWAY);
    }

    /**
     * Handle statement authorization exception response entity.
     *
     * @param exception the exception
     * @param request   the request
     * @return the response entity
     */
    @ExceptionHandler({ StatementAuthorizationException.class })
    public ResponseEntity<ExceptionDTO> handleStatementAuthorizationException(Exception exception,
        final HttpServletRequest request) {
        log.info("handleStatementAuthorizationException() - invoked");

        return new ResponseEntity<>(constructExceptionBody(exception, request, HttpStatus.UNAUTHORIZED),
            HttpStatus.UNAUTHORIZED);
    }

    private ExceptionDTO constructExceptionBody(final Exception exception, final HttpServletRequest request,
        final HttpStatus httpStatus) {

        ExceptionDTO exceptionDTO = new ExceptionDTO();

        exceptionDTO.setTimestamp(new Date());
        exceptionDTO.setException(exception.getClass().getName());
        exceptionDTO.setMessage(exception.getMessage());
        if (request != null) {
            exceptionDTO.setPath(request.getServletPath());
        }
        exceptionDTO.setStatusCode(httpStatus);

        return exceptionDTO;
    }
}
