package com.nagaroo.statement.service.composite.controller;

import com.nagaroo.statement.service.core.dto.StatementResponseDTO;
import com.nagaroo.statement.service.core.service.StatementService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * The type Statement controller.
 */
@Slf4j
@RestController
@RequestMapping("/statement-management/v1")
@Tag(name = "Statement",
    description = "The Statement API")
public class StatementController {

    /**
     * The Statement service.
     */
    private final StatementService statementService;

    /**
     * Instantiates a new Statement controller.
     *
     * @param statementService the statement service
     */
    public StatementController(final StatementService statementService) {
        this.statementService = statementService;
    }

    /**
     * Gets statements.
     *
     * @param accountID     the account id
     * @param requestParams the request params
     * @param request       the request
     * @return the statements
     */
    @Operation(summary = "Get Statements by Account ID",
        description = "Get a statements by `accountID`. " + "For an exact date range, use `fromDate` and `toDate` instead as query parameters. " + "For an exact amount range, use `fromAmount` and `toAmount` instead.",
        tags = { "Statement" })
    @ApiResponses(value = { @ApiResponse(responseCode = "200",
        description = "Ok",
        content = @Content(schema = @Schema(implementation = StatementResponseDTO.class))),
        @ApiResponse(responseCode = "400",
            description = "Invalid Input Data"), @ApiResponse(responseCode = "401",
        description = "Unauthorized Access to API"), @ApiResponse(responseCode = "404",
        description = "Requested Data Not Found In DB") })
    @ApiImplicitParams({ @ApiImplicitParam(name = "fromDate",
        value = "The data parameter to start the date range search. Must be used together with `toDate`. `fromDate` should be a past date than `toDate`. This parameter is incompatible with `fromAmount` or `toAmount`.",
        dataTypeClass = String.class,
        example = "dd-mm-yyyy",
        paramType = "query"), @ApiImplicitParam(name = "toDate",
        value = "The data parameter to end the date range search. Must be used together with `fromDate`. `toDate` should be a future date than `fromDate`. This parameter is incompatible with `fromAmount` or `toAmount`.",
        example = "dd-mm-yyyy",
        dataTypeClass = String.class,
        paramType = "query"), @ApiImplicitParam(name = "fromAmount",
        value = "The amount parameter to start the amount range search. Must be used together with `toAmount`. `fromAmount` should be a smaller value than `toAmount` and it cannot be a minus value. This parameter is incompatible with `fromDate` or `toDate`.",
        dataTypeClass = Double.class,
        example = "100",
        paramType = "query"), @ApiImplicitParam(name = "toAmount",
        value = "The amount parameter to end the amount range search. Must be used together with `fromAmount`. `toAmount` should be a lager value than `fromAmount` and it cannot be a minus value. This parameter is incompatible with `fromDate` or `toDate`.",
        dataTypeClass = Double.class,
        example = "1000",
        paramType = "query") })
    @GetMapping(value = "/accounts/{accountID}",
        produces = { "application/json" })
    public ResponseEntity<StatementResponseDTO> getStatements(
        @Parameter(description = "The Account ID of the user to fetch statement details")
        @PathVariable(value = "accountID") int accountID,
        @ApiIgnore @RequestParam(required = false) Map<String, Object> requestParams, HttpServletRequest request) {
        log.info("getStatements() - accountID : {}, requestParams: {} ", accountID, requestParams);

        StatementResponseDTO dto = statementService.getStatements(accountID, requestParams, request);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }


}
