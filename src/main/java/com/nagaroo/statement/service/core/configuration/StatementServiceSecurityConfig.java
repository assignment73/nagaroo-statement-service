package com.nagaroo.statement.service.core.configuration;

import com.nagaroo.statement.service.core.exception.StatementAuthorizationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import static com.nagaroo.statement.service.core.enums.RoleType.ADMIN;
import static com.nagaroo.statement.service.core.enums.RoleType.USER;

/**
 * The type Statement service security config.
 */
@Slf4j
@Configuration
@EnableWebSecurity
public class StatementServiceSecurityConfig {

    /**
     * User details service in memory user details manager.
     *
     * @return the in memory user details manager
     */
    @Bean
    public InMemoryUserDetailsManager userDetailsService() {

        InMemoryUserDetailsManager userDetailsService = new InMemoryUserDetailsManager();
        UserDetails admin = User.withUsername("admin").password(passwordEncoder().encode("admin")).roles(ADMIN.name())
            .build();
        UserDetails user = User.withUsername("user").password(passwordEncoder().encode("user")).roles(USER.name())
            .build();
        userDetailsService.createUser(admin);
        userDetailsService.createUser(user);
        return userDetailsService;
    }

    /**
     * Default security filter chain security filter chain.
     *
     * @param http the http
     * @return the security filter chain
     * @throws Exception the exception
     */
    @Bean
    SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(auth -> {
            try {
                auth.antMatchers("/statement-management/v1/accounts/{accountID}").hasAnyRole(USER.name(), ADMIN.name())
                    .and().formLogin().and().sessionManagement().maximumSessions(1).expiredUrl("/logout");
            } catch (Exception e) {
                throw new StatementAuthorizationException(
                    String.format("\"defaultSecurityFilterChain() - http : %s, exception : %s", http, e));
            }
        }).httpBasic(Customizer.withDefaults());
        return http.build();
    }

    /**
     * Password encoder password encoder.
     *
     * @return the password encoder
     */
    @Bean
    //password encoder
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
