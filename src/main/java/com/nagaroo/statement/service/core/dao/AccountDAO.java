package com.nagaroo.statement.service.core.dao;

import com.nagaroo.statement.service.core.dto.AccountDTO;

/**
 * The interface Account dao.
 */
public interface AccountDAO {

    /**
     * Gets account by account id.
     *
     * @param accountID the account id
     * @return the account by account id
     */
    AccountDTO getAccountByAccountId(final int accountID);

}
