package com.nagaroo.statement.service.core.dao;

import com.nagaroo.statement.service.core.dto.AccountDTO;
import com.nagaroo.statement.service.core.exception.ItemNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;

/**
 * The type Account dao.
 */
@Slf4j
@Service
public class AccountDAOImpl implements AccountDAO {

    private static final String ACCOUNT_SELECT_ALL = "SELECT * FROM account WHERE {0}={1}";
    private static final String ID = "ID";
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Instantiates a new Account dao.
     *
     * @param jdbcTemplate the jdbc template
     */
    public AccountDAOImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public AccountDTO getAccountByAccountId(final int accountID) {
        log.info("getAccountByAccountId() - accountID : {} ", accountID);
        String query = MessageFormat.format(ACCOUNT_SELECT_ALL, ID, accountID);

        try {
            return jdbcTemplate.queryForObject(query,
                (rs, rowNum) -> new AccountDTO(rs.getInt("ID"), rs.getString("account_type"),
                    rs.getString("account_number")));
        } catch (EmptyResultDataAccessException e) {
            throw new ItemNotFoundException("No result found for Account ID: " + accountID, e.fillInStackTrace());
        }
    }
}
