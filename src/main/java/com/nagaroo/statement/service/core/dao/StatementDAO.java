package com.nagaroo.statement.service.core.dao;

import com.nagaroo.statement.service.core.dto.StatementDTO;

import java.util.List;

/**
 * The interface Statement dao.
 */
public interface StatementDAO {

    /**
     * Gets statements by account id.
     *
     * @param accountID the account id
     * @return the statements by account id
     */
    List<StatementDTO> getStatementsByAccountId(final int accountID);

}
