package com.nagaroo.statement.service.core.dao;

import com.nagaroo.statement.service.core.dto.StatementDTO;
import com.nagaroo.statement.service.core.exception.InvalidIDataException;
import com.nagaroo.statement.service.core.util.Constants;
import com.nagaroo.statement.service.core.util.DateFormatValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.time.DateTimeException;
import java.util.List;

/**
 * The type Statement dao.
 */
@Slf4j
@Service
public class StatementDAOImpl implements StatementDAO {

    private static final String STATEMENT_SELECT_ALL = "SELECT * FROM statement WHERE {0}={1}";
    private static final String ACCOUNT_ID = "account_id";
    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Instantiates a new Statement dao.
     *
     * @param jdbcTemplate the jdbc template
     */
    public StatementDAOImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<StatementDTO> getStatementsByAccountId(final int accountID) {
        log.info("getStatementsByAccountId() - accountID : {} ", accountID);
        String query = MessageFormat.format(STATEMENT_SELECT_ALL, ACCOUNT_ID, accountID);

        return jdbcTemplate.query(query, (rs, rowNum) -> {
            try {
                return new StatementDTO(rs.getInt("ID"), rs.getString(ACCOUNT_ID), DateFormatValidator
                    .checkValidDate(Constants.DATE_FORMAT_DAY_MONTH_YEAR,
                        rs.getString("datefield").replace(Constants.DOT, Constants.DASH)),
                    Double.parseDouble(rs.getString("amount")));
            } catch (DateTimeException e) {
                throw new InvalidIDataException(
                    "MS access DB date format not supported for date : " + rs.getString("datefield"),
                    e.fillInStackTrace());
            }
        });
    }

}