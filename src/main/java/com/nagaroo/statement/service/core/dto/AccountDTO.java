package com.nagaroo.statement.service.core.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The type Account dto.
 */
@Entity
@Table(name = "account")
@Getter
@Setter
@NoArgsConstructor
public class AccountDTO {

    @Schema(description = "Unique identifier of the Account.",
        example = "1",
        required = true)
    @Id
    @Column(name = "ID")
    private int id;

    @Schema(description = "Type of the account such as savings or currunt",
        example = "savings",
        required = true)
    @Column(name = "account_type")
    private String accountType;

    @Schema(description = "The bank account number",
        example = "1234567891234",
        required = true)
    @Column(name = "account_number")
    private String accountNumber;

    /**
     * Instantiates a new Account dto.
     *
     * @param ID             the id
     * @param account_type   the account type
     * @param account_number the account number
     */
    public AccountDTO(final int ID, final String account_type, final String account_number) {
        this.id = ID;
        this.accountType = account_type;
        this.accountNumber = account_number;
    }
}
