package com.nagaroo.statement.service.core.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.Date;

/**
 * The type Exception dto.
 */
@Data
@Setter
public class ExceptionDTO {

    /**
     * The Timestamp.
     */
    @Schema(description = "The time stamp of the exception.")
    public Date timestamp;

    /**
     * The Message.
     */
    @Schema(description = "The exception message.")
    public String message;

    /**
     * The Status code.
     */
    @Schema(description = "The HTTP Status code of the exception")
    public HttpStatus statusCode;

    /**
     * The Exception.
     */
    @Schema(description = "Type of exception thrown")
    public String exception;

    /**
     * The Path.
     */
    @Schema(description = "The request path of the exception")
    public String path;

}
