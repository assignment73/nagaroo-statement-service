package com.nagaroo.statement.service.core.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

/**
 * The type Statement dto.
 */
@Entity
@Table(name = "statement")
@Getter
@Setter
@NoArgsConstructor
public class StatementDTO {

    @Id
    @Schema(description = "Unique identifier of the Account record in.",
        example = "1",
        required = true)
    @Column(name = "ID")
    private int id;

    @Schema(description = "Unique identifier of the Account.",
        example = "1",
        required = true)
    @Column(name = "account_id")
    private String accountId;

    @Schema(description = "Transaction Date",
        example = "03-09-2020")
    @Column(name = "datefield")
    private LocalDate dateField;

    @Schema(description = "Transaction Amount",
        example = "967.410308637791")
    @Column(name = "amount")
    private double amount;

    /**
     * Instantiates a new Statement dto.
     *
     * @param ID        the id
     * @param accountId the account id
     * @param dateField the date field
     * @param amount    the amount
     */
    public StatementDTO(final int ID, final String accountId, final LocalDate dateField, final double amount) {
        this.id = ID;
        this.accountId = accountId;
        this.dateField = dateField;
        this.amount = amount;
    }
}
