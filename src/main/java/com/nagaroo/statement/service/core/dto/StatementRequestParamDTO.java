package com.nagaroo.statement.service.core.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The type Statement request param dto.
 */
@Data
@NoArgsConstructor
public class StatementRequestParamDTO {

    private String fromDate;

    private String toDate;

    private String fromAmount;

    private String toAmount;

    /**
     * Instantiates a new Statement request param dto.
     *
     * @param fromDate   the from date
     * @param toDate     the to date
     * @param fromAmount the from amount
     * @param toAmount   the to amount
     */
    public StatementRequestParamDTO(final String fromDate, final String toDate, final String fromAmount,
        final String toAmount) {
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.fromAmount = fromAmount;
        this.toAmount = toAmount;
    }
}
