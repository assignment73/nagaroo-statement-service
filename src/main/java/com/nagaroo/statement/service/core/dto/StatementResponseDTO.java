package com.nagaroo.statement.service.core.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * The type Statement response dto.
 */
@Getter
@Setter
@NoArgsConstructor
public class StatementResponseDTO {

    private AccountDTO accountDetails;

    private List<StatementDTO> statementList;

    /**
     * Instantiates a new Statement response dto.
     *
     * @param accountDetails the account details
     * @param statementList  the statement list
     */
    public StatementResponseDTO(final AccountDTO accountDetails, final List<StatementDTO> statementList) {
        this.accountDetails = accountDetails;
        this.statementList = statementList;
    }
}
