package com.nagaroo.statement.service.core.enums;

/**
 * The enum Get statement request parameters.
 */
// whitelisted RequestParameters for get statements request
public enum GetStatementRequestParameters {

    /**
     * To date get statement request parameters.
     */
    TO_DATE("toDate"),
    /**
     * From date get statement request parameters.
     */
    FROM_DATE("fromDate"),
    /**
     * To amount get statement request parameters.
     */
    TO_AMOUNT("toAmount"),
    /**
     * From amount get statement request parameters.
     */
    FROM_AMOUNT("fromAmount");

    private String paramKey;

    GetStatementRequestParameters(final String paramKey) {
        this.paramKey = paramKey;
    }

    /**
     * Gets param key.
     *
     * @return the param key
     */
    public String getParamKey() {
        return paramKey;
    }


}
