package com.nagaroo.statement.service.core.enums;

/**
 * The enum Request type.
 */
public enum RequestType {

    /**
     * Date range request type.
     */
    DATE_RANGE,
    /**
     * Amount range request type.
     */
    AMOUNT_RANGE,
    /**
     * Default request type.
     */
    DEFAULT
}
