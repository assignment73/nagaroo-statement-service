package com.nagaroo.statement.service.core.enums;

/**
 * The enum Role type.
 */
public enum RoleType {

    /**
     * Admin role type.
     */
    ADMIN,
    /**
     * User role type.
     */
    USER
}
