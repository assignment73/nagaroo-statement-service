package com.nagaroo.statement.service.core.exception;

/**
 * The type Invalid i data exception.
 */
public class InvalidIDataException extends RuntimeException {

    /**
     * Instantiates a new Invalid i data exception.
     *
     * @param errorMessage the error message
     * @param throwable    the throwable
     */
    public InvalidIDataException(final String errorMessage, final Throwable throwable) {
        super(errorMessage, throwable);
    }
}
