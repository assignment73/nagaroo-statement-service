package com.nagaroo.statement.service.core.exception;

/**
 * The type Item not found exception.
 */
public class ItemNotFoundException extends RuntimeException {

    /**
     * Instantiates a new Item not found exception.
     *
     * @param errorMessage the error message
     * @param throwable    the throwable
     */
    public ItemNotFoundException(final String errorMessage, final Throwable throwable) {
        super(errorMessage, throwable);
    }
}
