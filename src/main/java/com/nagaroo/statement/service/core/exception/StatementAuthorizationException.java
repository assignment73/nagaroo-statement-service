package com.nagaroo.statement.service.core.exception;

/**
 * The type Statement authorization exception.
 */
public class StatementAuthorizationException extends RuntimeException {

    /**
     * Instantiates a new Statement authorization exception.
     *
     * @param errorMessage the error message
     */
    public StatementAuthorizationException(final String errorMessage) {
        super(errorMessage);
    }

}
