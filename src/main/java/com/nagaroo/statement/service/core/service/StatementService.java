package com.nagaroo.statement.service.core.service;

import com.nagaroo.statement.service.core.dto.StatementResponseDTO;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * The interface Statement service.
 */
public interface StatementService {

    /**
     * Gets statements.
     *
     * @param accountID     the account id
     * @param requestParams the request params
     * @param request       the request
     * @return the statements
     */
    StatementResponseDTO getStatements(final int accountID, Map<String, Object> requestParams,
        final HttpServletRequest request);
}
