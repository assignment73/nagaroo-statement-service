package com.nagaroo.statement.service.core.service;

import com.nagaroo.statement.service.core.dao.AccountDAO;
import com.nagaroo.statement.service.core.dao.StatementDAO;
import com.nagaroo.statement.service.core.dto.AccountDTO;
import com.nagaroo.statement.service.core.dto.StatementDTO;
import com.nagaroo.statement.service.core.dto.StatementRequestParamDTO;
import com.nagaroo.statement.service.core.dto.StatementResponseDTO;
import com.nagaroo.statement.service.core.enums.GetStatementRequestParameters;
import com.nagaroo.statement.service.core.enums.RequestType;
import com.nagaroo.statement.service.core.exception.InvalidInputDataException;
import com.nagaroo.statement.service.core.exception.StatementAuthorizationException;
import com.nagaroo.statement.service.core.util.Constants;
import com.nagaroo.statement.service.core.util.DateFormatValidator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

/**
 * The type Statement service.
 */
@Slf4j
@Service
public class StatementServiceImpl implements StatementService {

    /**
     * The Months.
     */
    @Value("${com.nagaroo.statement.service.configuration.months}")
    public int months;

    private StatementDAO statementDAO;

    private AccountDAO accountDAO;

    private LocalDate fromDateParam;

    private LocalDate toDateParam;

    private double fromAmountParam;

    private double toAmountParam;

    private StatementRequestParamDTO statementRequestParamDTO = new StatementRequestParamDTO();

    /**
     * Instantiates a new Statement service.
     *
     * @param statementDAO the statement dao
     * @param accountDAO   the account dao
     */
    public StatementServiceImpl(final StatementDAO statementDAO, final AccountDAO accountDAO) {
        this.statementDAO = statementDAO;
        this.accountDAO = accountDAO;
    }

    @Override
    public StatementResponseDTO getStatements(final int accountID, final Map<String, Object> requestParams,
        final HttpServletRequest request) {

        // Check if any test user tries to specify any parameter
        validateRequestForUserRole(requestParams, request);

        boolean isValidRequestParam = false;

        // Check if the request parameters are invalid
        if (!requestParams.isEmpty()) {
            isValidRequestParam = isValidRequestParam(requestParams);
        }

        RequestType requestType = RequestType.DEFAULT;

        // validate request params only if available
        if (isValidRequestParam) {
            requestType = validateInputData(statementRequestParamDTO.getFromDate(),
                statementRequestParamDTO.getToDate(), statementRequestParamDTO.getFromAmount(),
                statementRequestParamDTO.getToAmount());
        }
        log.info("getStatements() - Request Type for accountID : {} is requestType : {} ", accountID, requestType);

        //Get Statement Details form 'account' table
        AccountDTO accountDTO = accountDAO.getAccountByAccountId(accountID);

        //Get Statement Details form 'statement' table
        List<StatementDTO> statementDTOList = statementDAO.getStatementsByAccountId(accountID);

        //Process the statement result set according to the search criteria
        switch (requestType) {
        case DATE_RANGE:
            statementDTOList = getDateRangeStatementResult(statementDTOList, fromDateParam, toDateParam);
            break;
        case AMOUNT_RANGE:
            statementDTOList = getAccountRangeStatementResult(statementDTOList, fromAmountParam, toAmountParam);
            break;
        case DEFAULT:
        default:
            statementDTOList = getDefaultStatementResult(statementDTOList);
        }

        //hash the account number
        accountDTO.setAccountNumber(DigestUtils.sha256Hex(accountDTO.getAccountNumber()));

        return new StatementResponseDTO(accountDTO, statementDTOList);

    }

    private void validateRequestForUserRole(final Map<String, Object> requestParams, final HttpServletRequest request) {

        if (request == null) {
            throw new StatementAuthorizationException("Unknown user role!");
        } // requestParams default value is set to empty at the controller. therefore its not null
        else if (!requestParams.isEmpty() && request.isUserInRole("ROLE_USER")) {

            throw new StatementAuthorizationException(String
                .format("User unauthorized to specify any parameters. Additional parameters : %s", requestParams));
        }
    }

    private boolean isValidRequestParam(Map<String, Object> requestParams) {

        int originalRequestParamsSize = requestParams.size();

        BiFunction<String, Map<String, Object>, String> remappingFunction = (k, v) -> v != null ? v.remove(k)
            .toString() : null;

        statementRequestParamDTO.setFromDate(
            requestParams.containsKey(GetStatementRequestParameters.FROM_DATE.getParamKey()) ? remappingFunction
                .apply(GetStatementRequestParameters.FROM_DATE.getParamKey(), requestParams) : "");
        statementRequestParamDTO.setToDate(
            requestParams.containsKey(GetStatementRequestParameters.TO_DATE.getParamKey()) ? remappingFunction
                .apply(GetStatementRequestParameters.TO_DATE.getParamKey(), requestParams) : "");
        statementRequestParamDTO.setFromAmount(
            requestParams.containsKey(GetStatementRequestParameters.FROM_AMOUNT.getParamKey()) ? remappingFunction
                .apply(GetStatementRequestParameters.FROM_AMOUNT.getParamKey(), requestParams) : "");
        statementRequestParamDTO.setToAmount(
            requestParams.containsKey(GetStatementRequestParameters.TO_AMOUNT.getParamKey()) ? remappingFunction
                .apply(GetStatementRequestParameters.TO_AMOUNT.getParamKey(), requestParams) : "");

        if (!requestParams.isEmpty()) {
            throw new InvalidInputDataException(
                String.format("Invalid query parameters added. RequestParams : %s", requestParams));
        }

        return originalRequestParamsSize > requestParams.size();
    }

    private List<StatementDTO> getDateRangeStatementResult(final List<StatementDTO> statementDTOList,
        final LocalDate fromDate, final LocalDate toDate) {
        List<StatementDTO> dateRangeStatementDTOList = new ArrayList<>();

        for (StatementDTO statementDTO : statementDTOList) {
            if (statementDTO.getDateField().isAfter(fromDate) && statementDTO.getDateField().isBefore(toDate)) {
                dateRangeStatementDTOList.add(statementDTO);
            }
        }
        return dateRangeStatementDTOList;
    }

    private List<StatementDTO> getAccountRangeStatementResult(final List<StatementDTO> statementDTOList,
        final double fromAmount, final double toAmount) {
        List<StatementDTO> accountRangeStatementDTOList = new ArrayList<>();

        for (StatementDTO statementDTO : statementDTOList) {
            if (statementDTO.getAmount() > fromAmount && statementDTO.getAmount() < toAmount) {
                accountRangeStatementDTOList.add(statementDTO);
            }
        }
        return accountRangeStatementDTOList;
    }

    private List<StatementDTO> getDefaultStatementResult(final List<StatementDTO> statementDTOList) {
        List<StatementDTO> defaultStatementDTOList = new ArrayList<>();

        LocalDate pastDate = DateFormatValidator.getPastDate(months);

        for (StatementDTO statementDTO : statementDTOList) {
            if (statementDTO.getDateField().isAfter(pastDate)) {
                defaultStatementDTOList.add(statementDTO);
            }
        }

        return defaultStatementDTOList;
    }

    private RequestType validateInputData(final String fromDate, final String toDate, final String fromAmount,
        final String toAmount) {

        RequestType requestType = RequestType.DEFAULT;

        if (!fromDate.isEmpty() && !toDate.isEmpty()) {
            validateDateParameters(fromDate, toDate);
            requestType = RequestType.DATE_RANGE;
        } else if (!fromAmount.isEmpty() && !toAmount.isEmpty()) {
            validateAmountParameters(fromAmount, toAmount);
            requestType = RequestType.AMOUNT_RANGE;
        }

        return requestType;
    }

    private void validateAmountParameters(final String fromAmount, final String toAmount) {

        try {
            fromAmountParam = Double.parseDouble(fromAmount);
            toAmountParam = Double.parseDouble(toAmount);
        } catch (NumberFormatException e) {
            throw new InvalidInputDataException(String
                .format("Invalid Amount Format.  fromAmount : %s and toAmount : %s Not Exists", fromAmount, toAmount));
        }


        if (fromAmountParam < 0 || toAmountParam < 0) {

            throw new InvalidInputDataException(String.format(
                "Invalid Amount Parameters. Amount parameters cannot be minus. fromAmountParam : %s, toAmountParam : %s",
                fromAmountParam, toAmountParam));
        }

        if (fromAmountParam > toAmountParam) {
            throw new InvalidInputDataException(String
                .format("Invalid Amount Range for fromAmountParam : %s toAmountParam : %s", fromAmountParam,
                    toAmountParam));
        }
        log.info("validateAmountParameters() - Input Amount parameters are valid. fromAmount : {}, toAmount : {} ",
            fromAmount, toAmount);
    }

    private void validateDateParameters(final String fromDate, final String toDate) {
        try {
            this.fromDateParam = DateFormatValidator.checkValidDate(Constants.DATE_FORMAT_DAY_MONTH_YEAR, fromDate);
            this.toDateParam = DateFormatValidator.checkValidDate(Constants.DATE_FORMAT_DAY_MONTH_YEAR, toDate);
            log.info(
                "validateDateParameters() - Input Date parameters are valid. fromDateParam : {}, toDateParam : {} ",
                fromDateParam, toDateParam);
        } catch (DateTimeParseException e) {
            throw new InvalidInputDataException(String
                .format("Date Range parameters values not valid for fromDate : %s toDate : %s", fromDate, toDate));
        }

        if (!DateFormatValidator.isValidDateRange(fromDateParam, toDateParam)) {
            throw new InvalidInputDataException(String
                .format("Invalid Date Range for fromDateParam : %s toDateParam : %s", fromDateParam, toDateParam));
        }
    }
}
