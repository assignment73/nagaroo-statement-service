package com.nagaroo.statement.service.core.util;

/**
 * The type Constants.
 */
public final class Constants {

    /**
     * The constant DATE_FORMAT_DAY_MONTH_YEAR.
     */
    public static final String DATE_FORMAT_DAY_MONTH_YEAR = "dd-MM-yyyy";
    /**
     * The constant DASH.
     */
    public static final String DASH = "-";
    /**
     * The constant DOT.
     */
    public static final String DOT = ".";

    private Constants() {

    }
}
