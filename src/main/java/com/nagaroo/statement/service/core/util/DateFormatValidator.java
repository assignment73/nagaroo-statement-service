package com.nagaroo.statement.service.core.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

/**
 * The type Date format validator.
 */
public final class DateFormatValidator {

    private DateFormatValidator() {

    }

    /**
     * Check valid date local date.
     *
     * @param dateFormat the date format
     * @param date       the date
     * @return the local date
     */
    public static LocalDate checkValidDate(final String dateFormat, final String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat, Locale.ENGLISH);
        return LocalDate.parse(date, formatter);
    }

    /**
     * Is valid date range boolean.
     *
     * @param fromDate the from date
     * @param toDate   the to date
     * @return the boolean
     */
    public static boolean isValidDateRange(final LocalDate fromDate, final LocalDate toDate) {
        return fromDate.isBefore(toDate);
    }

    /**
     * Gets past date.
     *
     * @param months the months
     * @return the past date
     */
    public static LocalDate getPastDate(final int months) {
        LocalDate localDate = LocalDate.now();

        return localDate.minusMonths(months);
    }
}
