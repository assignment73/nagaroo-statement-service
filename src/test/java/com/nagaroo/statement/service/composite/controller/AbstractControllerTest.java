package com.nagaroo.statement.service.composite.controller;

import com.google.gson.Gson;
import com.nagaroo.statement.service.core.exception.InvalidIDataException;
import com.nagaroo.statement.service.core.exception.InvalidInputDataException;
import com.nagaroo.statement.service.core.exception.ItemNotFoundException;
import com.nagaroo.statement.service.core.exception.StatementAuthorizationException;
import com.nagaroo.statement.service.core.service.StatementService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
class AbstractControllerTest {

    private static final String MESSAGE = "message";
    MockMvc mockMvc;
    @InjectMocks
    StatementController statementController;
    @Mock
    StatementService statementService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(statementController).setControllerAdvice(AbstractController.class)
            .build();
    }

    @Test
    void handleBadRequestExceptions() throws Exception {
        MultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();
        String exceptionMessage = "AbstractController test for InvalidInputDataException";

        when(statementService.getStatements(anyInt(), anyMap(), any(HttpServletRequest.class)))
            .thenThrow(new InvalidInputDataException(exceptionMessage));

        MvcResult result = mockMvc
            .perform(MockMvcRequestBuilders.get("/statement-management/v1/accounts/2").queryParams(requestParams))
            .andExpect(status().isBadRequest()).andReturn();

        Map actualResponse = new Gson().fromJson(result.getResponse().getContentAsString(), Map.class);
        Assertions.assertEquals(actualResponse.get(MESSAGE), (exceptionMessage));
    }

    @Test
    void handleInvalidIDataException() throws Exception {
        MultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();
        String exceptionMessage = "handleInvalidIDataException() - invoked";

        when(statementService.getStatements(anyInt(), anyMap(), any(HttpServletRequest.class)))
            .thenThrow(new InvalidIDataException(exceptionMessage, null));

        MvcResult result = mockMvc
            .perform(MockMvcRequestBuilders.get("/statement-management/v1/accounts/2").queryParams(requestParams))
            .andExpect(status().isUnprocessableEntity()).andReturn();

        Map actualResponse = new Gson().fromJson(result.getResponse().getContentAsString(), Map.class);
        Assertions.assertEquals(actualResponse.get(MESSAGE), exceptionMessage);
    }

    @Test
    void handleItemNotFoundException() throws Exception {
        String exceptionMessage = "AbstractController test for ItemNotFoundException";
        MultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();

        when(statementService.getStatements(anyInt(), anyMap(), any(HttpServletRequest.class)))
            .thenThrow(new ItemNotFoundException(exceptionMessage, null));

        MvcResult result = mockMvc
            .perform(MockMvcRequestBuilders.get("/statement-management/v1/accounts/2").queryParams(requestParams))
            .andExpect(status().isNotFound()).andReturn();

        Map actualResponse = new Gson().fromJson(result.getResponse().getContentAsString(), Map.class);
        Assertions.assertEquals(actualResponse.get(MESSAGE), exceptionMessage);
    }

    @Test
    void handleStatementAuthorizationException() throws Exception {

        String exceptionMessage = "handleStatementAuthorizationException() - invoked";
        MultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();

        when(statementService.getStatements(anyInt(), anyMap(), any(HttpServletRequest.class)))
            .thenThrow(new StatementAuthorizationException(exceptionMessage));

        MvcResult result = mockMvc
            .perform(MockMvcRequestBuilders.get("/statement-management/v1/accounts/2").queryParams(requestParams))
            .andExpect(status().isUnauthorized()).andReturn();

        Map actualResponse = new Gson().fromJson(result.getResponse().getContentAsString(), Map.class);
        Assertions.assertEquals(actualResponse.get(MESSAGE), exceptionMessage);
    }
}