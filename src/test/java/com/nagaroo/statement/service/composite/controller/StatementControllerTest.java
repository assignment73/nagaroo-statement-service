package com.nagaroo.statement.service.composite.controller;

import com.nagaroo.statement.service.core.dto.AccountDTO;
import com.nagaroo.statement.service.core.dto.StatementDTO;
import com.nagaroo.statement.service.core.dto.StatementResponseDTO;
import com.nagaroo.statement.service.core.service.StatementService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
class StatementControllerTest {

    MockMvc mockMvc;

    @InjectMocks
    StatementController statementController;

    @Mock
    StatementService statementService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(statementController).setControllerAdvice(AbstractController.class)
            .build();
    }

    @Test
    void getStatements() throws Exception {
        MultiValueMap<String, String> requestParams = new LinkedMultiValueMap<>();
        requestParams.add("a", "b");

        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setAccountNumber("abc");
        accountDTO.setAccountType("AccountType");
        accountDTO.setId(1);

        StatementDTO statementDTO = new StatementDTO();
        statementDTO.setAccountId("1");
        statementDTO.setAmount(500);
        statementDTO.setDateField(LocalDate.now().minusMonths(2));
        statementDTO.setId(1);

        List<StatementDTO> statementDTOList = new ArrayList<>();
        statementDTOList.add(statementDTO);

        StatementResponseDTO statementResponseDTOResponse = new StatementResponseDTO();
        statementResponseDTOResponse.setAccountDetails(accountDTO);
        statementResponseDTOResponse.setStatementList(statementDTOList);

        when(statementService.getStatements(anyInt(), anyMap(), any(HttpServletRequest.class)))
            .thenReturn(statementResponseDTOResponse);

        mockMvc.perform(MockMvcRequestBuilders.get("/statement-management/v1/accounts/2").queryParams(requestParams))
            .andExpect(status().isOk()).andReturn();
    }

}