package com.nagaroo.statement.service.core.dao;

import com.nagaroo.statement.service.core.dto.AccountDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AccountDAOImplTest {

    @Mock
    JdbcTemplate jdbcTemplate;
    @InjectMocks
    private AccountDAOImpl accountDAO;

    @Test
    void getAccountByAccountId() {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setAccountNumber("abc");
        accountDTO.setAccountType("AccountType");
        accountDTO.setId(1);

        when(jdbcTemplate.queryForObject(Mockito.anyString(), Mockito.any(RowMapper.class))).thenReturn(accountDTO);

        AccountDTO response = accountDAO.getAccountByAccountId(1);
        Assertions.assertEquals(accountDTO.getAccountNumber(), response.getAccountNumber());
    }
}