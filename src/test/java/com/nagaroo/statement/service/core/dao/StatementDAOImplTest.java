package com.nagaroo.statement.service.core.dao;

import com.nagaroo.statement.service.core.dto.StatementDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class StatementDAOImplTest {

    @Mock
    JdbcTemplate jdbcTemplate;
    @InjectMocks
    private StatementDAOImpl statementDAO;

    @Test
    void getStatementsByAccountId() {
        StatementDTO statementDTO = new StatementDTO();
        statementDTO.setAccountId("1");
        statementDTO.setAmount(500);
        statementDTO.setDateField(LocalDate.now().minusMonths(2));
        statementDTO.setId(1);

        List<StatementDTO> statementDTOList = new ArrayList<>();
        statementDTOList.add(statementDTO);

        when(jdbcTemplate.query(Mockito.anyString(), Mockito.any(RowMapper.class))).thenReturn(statementDTOList);

        List<StatementDTO> response = statementDAO.getStatementsByAccountId(1);
        Assertions.assertEquals(statementDTOList.get(0), response.get(0));
    }

}