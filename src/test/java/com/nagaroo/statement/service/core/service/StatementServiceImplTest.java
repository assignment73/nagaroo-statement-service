package com.nagaroo.statement.service.core.service;

import com.nagaroo.statement.service.core.dao.AccountDAO;
import com.nagaroo.statement.service.core.dao.StatementDAO;
import com.nagaroo.statement.service.core.dto.AccountDTO;
import com.nagaroo.statement.service.core.dto.StatementDTO;
import com.nagaroo.statement.service.core.dto.StatementResponseDTO;
import com.nagaroo.statement.service.core.exception.InvalidInputDataException;
import com.nagaroo.statement.service.core.exception.StatementAuthorizationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class StatementServiceImplTest {

    @InjectMocks
    private StatementServiceImpl statementService;

    @Mock
    private StatementDAO statementDAO;

    @Mock
    private AccountDAO accountDAO;

    @Mock
    private HttpServletRequest request;

    @BeforeEach
    void setUp() {
        ReflectionTestUtils.setField(statementService, "months", 3);
    }

    @Test
    void getStatementsWhenNullPrinciple() {
        StatementAuthorizationException thrown = Assertions.assertThrows(StatementAuthorizationException.class, () -> {
            statementService.getStatements(1, null, null);
        });

        Assertions.assertEquals("Unknown user role!", thrown.getMessage());
    }

    @Test
    void getStatementsRoleIsUser() {
        Map<String, Object> requestParams = new HashMap<>();
        requestParams.put("a", "b");
        when(request.isUserInRole("ROLE_USER")).thenReturn(true);

        StatementAuthorizationException thrown = Assertions.assertThrows(StatementAuthorizationException.class, () -> {

            statementService.getStatements(1, requestParams, request);
        });

        Assertions.assertEquals("User unauthorized to specify any parameters. Additional parameters : {a=b}",
            thrown.getMessage());
    }

    @Test
    void getStatementsInvalidRequestParams() {
        Map<String, Object> requestParams = new HashMap<>();
        requestParams.put("a", "b");
        when(request.isUserInRole("ROLE_USER")).thenReturn(false);

        InvalidInputDataException thrown = Assertions.assertThrows(InvalidInputDataException.class, () -> {

            statementService.getStatements(1, requestParams, request);
        });

        Assertions.assertEquals("Invalid query parameters added. RequestParams : {a=b}", thrown.getMessage());
    }

    @Test
    void getStatements() {

        Map<String, Object> requestParams = Collections.emptyMap();

        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setAccountNumber("abc");
        accountDTO.setAccountType("AccountType");
        accountDTO.setId(1);

        StatementDTO statementDTO = new StatementDTO();
        statementDTO.setAccountId("1");
        statementDTO.setAmount(500);
        statementDTO.setDateField(LocalDate.now().minusMonths(2));
        statementDTO.setId(1);

        List<StatementDTO> statementDTOList = new ArrayList<>();
        statementDTOList.add(statementDTO);

        StatementResponseDTO statementResponseDTOResponse = new StatementResponseDTO();
        statementResponseDTOResponse.setAccountDetails(accountDTO);
        statementResponseDTOResponse.setStatementList(statementDTOList);

        when(statementDAO.getStatementsByAccountId(1)).thenReturn(statementDTOList);
        when(accountDAO.getAccountByAccountId(1)).thenReturn(accountDTO);

        Assertions.assertEquals(statementResponseDTOResponse.getStatementList(),
            statementService.getStatements(1, requestParams, request).getStatementList());

    }

    @Test
    void getStatementsForInvalidDateFormat() {

        Map<String, Object> requestParams = new HashMap<>();
        requestParams.put("toDate", "toDate");
        requestParams.put("fromDate", "20-02-2022");

        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setAccountNumber("abc");
        accountDTO.setAccountType("AccountType");
        accountDTO.setId(1);

        StatementDTO statementDTO = new StatementDTO();
        statementDTO.setAccountId("1");
        statementDTO.setAmount(500);
        statementDTO.setDateField(LocalDate.now().minusMonths(2));
        statementDTO.setId(1);

        List<StatementDTO> statementDTOList = new ArrayList<>();
        statementDTOList.add(statementDTO);

        StatementResponseDTO statementResponseDTOResponse = new StatementResponseDTO();
        statementResponseDTOResponse.setAccountDetails(accountDTO);
        statementResponseDTOResponse.setStatementList(statementDTOList);
        when(request.isUserInRole("ROLE_USER")).thenReturn(false);

        InvalidInputDataException thrown = Assertions.assertThrows(InvalidInputDataException.class, () -> {

            statementService.getStatements(1, requestParams, request);
        });

        Assertions.assertEquals("Date Range parameters values not valid for fromDate : 20-02-2022 toDate : toDate",
            thrown.getMessage());
    }

    @Test
    void getStatementsForInvalidDateRange() {

        Map<String, Object> requestParams = new HashMap<>();
        requestParams.put("toDate", "20-02-2021");
        requestParams.put("fromDate", "20-02-2022");

        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setAccountNumber("abc");
        accountDTO.setAccountType("AccountType");
        accountDTO.setId(1);

        StatementDTO statementDTO = new StatementDTO();
        statementDTO.setAccountId("1");
        statementDTO.setAmount(500);
        statementDTO.setDateField(LocalDate.now().minusMonths(2));
        statementDTO.setId(1);

        List<StatementDTO> statementDTOList = new ArrayList<>();
        statementDTOList.add(statementDTO);

        StatementResponseDTO statementResponseDTOResponse = new StatementResponseDTO();
        statementResponseDTOResponse.setAccountDetails(accountDTO);
        statementResponseDTOResponse.setStatementList(statementDTOList);
        when(request.isUserInRole("ROLE_USER")).thenReturn(false);

        InvalidInputDataException thrown = Assertions.assertThrows(InvalidInputDataException.class, () -> {

            statementService.getStatements(1, requestParams, request);
        });

        Assertions.assertEquals("Invalid Date Range for fromDateParam : 2022-02-20 toDateParam : 2021-02-20",
            thrown.getMessage());
    }

    @Test
    void getStatementsForInvalidAmountFomate() {

        Map<String, Object> requestParams = new HashMap<>();
        requestParams.put("toAmount", "toAmount");
        requestParams.put("fromAmount", "toAmount");

        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setAccountNumber("abc");
        accountDTO.setAccountType("AccountType");
        accountDTO.setId(1);

        StatementDTO statementDTO = new StatementDTO();
        statementDTO.setAccountId("1");
        statementDTO.setAmount(500);
        statementDTO.setDateField(LocalDate.now().minusMonths(2));
        statementDTO.setId(1);

        List<StatementDTO> statementDTOList = new ArrayList<>();
        statementDTOList.add(statementDTO);

        StatementResponseDTO statementResponseDTOResponse = new StatementResponseDTO();
        statementResponseDTOResponse.setAccountDetails(accountDTO);
        statementResponseDTOResponse.setStatementList(statementDTOList);
        when(request.isUserInRole("ROLE_USER")).thenReturn(false);

        InvalidInputDataException thrown = Assertions.assertThrows(InvalidInputDataException.class, () -> {

            statementService.getStatements(1, requestParams, request);
        });

        Assertions.assertEquals("Invalid Amount Format.  fromAmount : toAmount and toAmount : toAmount Not Exists",
            thrown.getMessage());
    }

    @Test
    void getStatementsForInvalidAmountRange() {

        Map<String, Object> requestParams = new HashMap<>();
        requestParams.put("toAmount", "10");
        requestParams.put("fromAmount", "100");

        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setAccountNumber("abc");
        accountDTO.setAccountType("AccountType");
        accountDTO.setId(1);

        StatementDTO statementDTO = new StatementDTO();
        statementDTO.setAccountId("1");
        statementDTO.setAmount(500);
        statementDTO.setDateField(LocalDate.now().minusMonths(2));
        statementDTO.setId(1);

        List<StatementDTO> statementDTOList = new ArrayList<>();
        statementDTOList.add(statementDTO);

        StatementResponseDTO statementResponseDTOResponse = new StatementResponseDTO();
        statementResponseDTOResponse.setAccountDetails(accountDTO);
        statementResponseDTOResponse.setStatementList(statementDTOList);
        when(request.isUserInRole("ROLE_USER")).thenReturn(false);

        InvalidInputDataException thrown = Assertions.assertThrows(InvalidInputDataException.class, () -> {

            statementService.getStatements(1, requestParams, request);
        });

        Assertions
            .assertEquals("Invalid Amount Range for fromAmountParam : 100.0 toAmountParam : 10.0", thrown.getMessage());
    }

    @Test
    void getStatementsForInvalidMinusAmountRange() {

        Map<String, Object> requestParams = new HashMap<>();
        requestParams.put("toAmount", "-10");
        requestParams.put("fromAmount", "100");

        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setAccountNumber("abc");
        accountDTO.setAccountType("AccountType");
        accountDTO.setId(1);

        StatementDTO statementDTO = new StatementDTO();
        statementDTO.setAccountId("1");
        statementDTO.setAmount(500);
        statementDTO.setDateField(LocalDate.now().minusMonths(2));
        statementDTO.setId(1);

        List<StatementDTO> statementDTOList = new ArrayList<>();
        statementDTOList.add(statementDTO);

        StatementResponseDTO statementResponseDTOResponse = new StatementResponseDTO();
        statementResponseDTOResponse.setAccountDetails(accountDTO);
        statementResponseDTOResponse.setStatementList(statementDTOList);
        when(request.isUserInRole("ROLE_USER")).thenReturn(false);

        InvalidInputDataException thrown = Assertions.assertThrows(InvalidInputDataException.class, () -> {

            statementService.getStatements(1, requestParams, request);
        });

        Assertions.assertEquals(
            "Invalid Amount Parameters. Amount parameters cannot be minus. fromAmountParam : 100.0, toAmountParam : -10.0",
            thrown.getMessage());
    }

    @Test
    void getStatementsByAmountRange() {

        Map<String, Object> requestParams = new HashMap<>();
        requestParams.put("toAmount", "1000");
        requestParams.put("fromAmount", "100");

        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setAccountNumber("abc");
        accountDTO.setAccountType("AccountType");
        accountDTO.setId(1);

        StatementDTO statementDTO = new StatementDTO();
        statementDTO.setAccountId("1");
        statementDTO.setAmount(500);
        statementDTO.setDateField(LocalDate.now().minusMonths(2));
        statementDTO.setId(1);

        List<StatementDTO> statementDTOList = new ArrayList<>();
        statementDTOList.add(statementDTO);

        StatementResponseDTO statementResponseDTOResponse = new StatementResponseDTO();
        statementResponseDTOResponse.setAccountDetails(accountDTO);
        statementResponseDTOResponse.setStatementList(statementDTOList);

        when(request.isUserInRole("ROLE_USER")).thenReturn(false);
        when(statementDAO.getStatementsByAccountId(1)).thenReturn(statementDTOList);
        when(accountDAO.getAccountByAccountId(1)).thenReturn(accountDTO);

        Assertions.assertEquals(statementResponseDTOResponse.getStatementList(),
            statementService.getStatements(1, requestParams, request).getStatementList());

    }


    @Test
    void getStatementsByDateRange() {
        Map<String, Object> requestParams = new HashMap<>();
        requestParams.put("toDate", "20-09-2022");
        requestParams.put("fromDate", "20-02-2018");

        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setAccountNumber("abc");
        accountDTO.setAccountType("AccountType");
        accountDTO.setId(1);

        StatementDTO statementDTO = new StatementDTO();
        statementDTO.setAccountId("1");
        statementDTO.setAmount(500);
        statementDTO.setDateField(LocalDate.now().minusMonths(5));
        statementDTO.setId(1);

        List<StatementDTO> statementDTOList = new ArrayList<>();
        statementDTOList.add(statementDTO);

        StatementResponseDTO statementResponseDTOResponse = new StatementResponseDTO();
        statementResponseDTOResponse.setAccountDetails(accountDTO);
        statementResponseDTOResponse.setStatementList(statementDTOList);

        when(request.isUserInRole("ROLE_USER")).thenReturn(false);
        when(statementDAO.getStatementsByAccountId(1)).thenReturn(statementDTOList);
        when(accountDAO.getAccountByAccountId(1)).thenReturn(accountDTO);

        Assertions.assertEquals(statementResponseDTOResponse.getStatementList(),
            statementService.getStatements(1, requestParams, request).getStatementList());
    }
}