package com.nagaroo.statement.service.core.util;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DateFormatValidatorTest {

    private String fromDate = "11-01-2012";
    private String toDate = "11-01-2020";

    @Test
    void checkValidDate() {
        LocalDate localDate = DateFormatValidator.checkValidDate(Constants.DATE_FORMAT_DAY_MONTH_YEAR, fromDate);
        assertEquals(fromDate,
            localDate.format(DateTimeFormatter.ofPattern(Constants.DATE_FORMAT_DAY_MONTH_YEAR, Locale.ENGLISH)));
    }

    @Test
    void isValidDateRange() {
        assertTrue(DateFormatValidator
            .isValidDateRange(DateFormatValidator.checkValidDate(Constants.DATE_FORMAT_DAY_MONTH_YEAR, fromDate),
                DateFormatValidator.checkValidDate(Constants.DATE_FORMAT_DAY_MONTH_YEAR, toDate)));

    }

    @Test
    void getPastDate() {
        LocalDate localDate = DateFormatValidator.getPastDate(3);
        assertEquals(LocalDate.now().minusMonths(3), localDate);
    }
}